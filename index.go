package noonhttp

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

const (
	dialerTimeout             = 30 * time.Second
	dialerKeepAlive           = 30 * time.Second
	maxConnectionPerHost      = 50
	maxIdleConnections        = 300
	maxIdleConnectionsPerHost = 50
)

type (
	// Config HttpConfig
	Config struct {
		MaxIdleConns        int
		MaxIdleConnsPerHost int
		MaxConnsPerHost     int
		Timeout             time.Duration
		DialerTimeout       time.Duration
		DialerKeepAlive     time.Duration
	}

	// ClientEntity Http Client entity struct
	ClientEntity struct {
		client *http.Client
	}
)

// Initialize initialize http client
func Initialize(config Config) *ClientEntity {
	config.manageConfig()

	transport := http.DefaultTransport.(*http.Transport)
	transport.MaxIdleConns = config.MaxIdleConns
	transport.MaxIdleConnsPerHost = config.MaxIdleConnsPerHost
	transport.MaxConnsPerHost = config.MaxConnsPerHost

	dialContext := (&net.Dialer{
		Timeout:   config.DialerTimeout,
		KeepAlive: config.DialerKeepAlive,
		DualStack: true,
	}).DialContext
	transport.DialContext = dialContext

	client := &http.Client{
		Transport: transport,
	}

	if config.Timeout != 0 {
		client.Timeout = config.Timeout
	}

	noonHTTPClientEntity := &ClientEntity{
		client: client,
	}
	return noonHTTPClientEntity
}

// ServePost serves POST request
func (entity *ClientEntity) ServePost(endpoint string, headers map[string]string, payload map[string]interface{}) ([]byte, error) {
	reqBody, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(reqBody))
	if err != nil {
		return nil, err
	}

	populateHeaders(request, headers)

	resp, err := entity.client.Do(request)
	if err != nil {
		return nil, &ServerError{
			StatusCode: 500,
			Message:    "client return status : " + err.Error(),
		}
	}

	if resp == nil {
		return nil, &ServerError{
			StatusCode: 500,
			Message:    "client return nil response",
		}
	}

	decodedByte, err := decodeResponse(resp)
	defer resp.Body.Close()

	return decodedByte, err
}

// ServeGet serves GET request
func (entity *ClientEntity) ServeGet(endpoint string, headers map[string]string, queryParams map[string]string) ([]byte, error) {
	request, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		return nil, err
	}
	populateHeaders(request, headers)

	q := request.URL.Query()
	for k, v := range queryParams {
		q.Add(k, v)
	}
	request.URL.RawQuery = q.Encode()

	resp, err := entity.client.Do(request)
	if err != nil {
		return nil, &ServerError{
			StatusCode: 500,
			Message:    "client return status : " + err.Error(),
		}
	}

	if resp == nil {
		return nil, &ServerError{
			StatusCode: 500,
			Message:    "client return nil response",
		}
	}

	decodedResponse, err := decodeResponse(resp)
	defer resp.Body.Close()

	return decodedResponse, err
}

func (entity *Config) manageConfig() {
	if entity.MaxConnsPerHost <= 0 {
		entity.MaxConnsPerHost = maxConnectionPerHost
	}
	if entity.MaxIdleConns <= 0 {
		entity.MaxIdleConns = maxIdleConnections
	}
	if entity.MaxIdleConnsPerHost <= 0 {
		entity.MaxIdleConnsPerHost = maxIdleConnectionsPerHost
	}
	if entity.DialerTimeout == 0 {
		entity.DialerTimeout = dialerTimeout
	}
	if entity.DialerKeepAlive == 0 {
		entity.DialerKeepAlive = dialerKeepAlive
	}
}

func populateHeaders(r *http.Request, headers map[string]string) {
	for key, value := range headers {
		r.Header.Add(key, value)
	}
}

func decodeResponse(resp *http.Response) ([]byte, error) {
	if resp.StatusCode < 400 {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		return body, nil
	}

	if resp.StatusCode >= 400 && resp.StatusCode < 500 {
		_, err := io.Copy(ioutil.Discard, resp.Body)
		if err != nil {
			return nil, err
		}
		return nil, &ClientError{
			StatusCode: resp.StatusCode,
			Message:    "client return Status : " + resp.Status,
		}
	}

	if resp.StatusCode >= 500 {
		_, err := io.Copy(ioutil.Discard, resp.Body)
		if err != nil {
			return nil, err
		}
		return nil, &ServerError{
			StatusCode: resp.StatusCode,
			Message:    "client return Status : " + resp.Status,
		}
	}
	return nil, errors.New("invalid response")
}
