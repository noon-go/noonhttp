package noonhttp

type ClientError struct {
	Message    string
	StatusCode int
}

type ServerError struct {
	Message    string
	StatusCode int
}

func (ce *ClientError) Error() string {
	return ce.Message
}

func (se *ServerError) Error() string {
	return se.Message
}
